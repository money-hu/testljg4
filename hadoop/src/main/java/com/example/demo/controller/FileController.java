package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import org.apache.hadoop.fs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/file")
public class FileController{
    @Autowired
    private FileSystem fs;
    // 上传文件
    @RequestMapping("/upload")
    public AjaxResult handleFileUpload(@RequestParam("file") MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());//获取到文件名
        Map<String,String> ret = new HashMap<>();
        try {
            // 判断文件是否存在
            Path filePath = new Path("/output/" + fileName);
            if (fs.exists(filePath)) {
                return AjaxResult.fail(-1,"文件已经存在，请勿重复上传！");
            }

            // 创建一个新的HDFS路径
            Path newFilePath = new Path("/output/" + fileName);
            // 将文件数据写入HDFS
            FSDataOutputStream os = fs.create(newFilePath);
            os.write(file.getBytes());
            os.close();

            // 获取文件大小和创建时间
            String fileSize = String.format("%.2f", (double) file.getSize() / 1024) + "KB";
            FileStatus fileStatus = fs.getFileStatus(newFilePath);
            long modificationTime = fileStatus.getModificationTime();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("Asia/Shanghai"));
            String uploadTime = formatter.format(Instant.ofEpochMilli(modificationTime));

            // 将文件名、文件大小、上传时间添加到AjaxResult中
            ret.put("fileName", fileName);
            ret.put("fileSize", fileSize);
            ret.put("uploadTime", uploadTime);

        } catch (Exception ex) {
            return AjaxResult.fail(-2,"上传文件失败！" + ex.getMessage());
        }

        return AjaxResult.success(ret);
    }
    // 下载文件
    @RequestMapping("/download")
    public void downloadFile(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        try {
            // 获取文件在 HDFS 上的路径
            Path filePath = new Path("/output/" + fileName);
            // 如果文件不存在，则直接返回
            if (!fs.exists(filePath)) {
                return;
            }

            // 设置响应头信息
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setContentType("application/octet-stream");

            // 从 HDFS 上读取文件数据
            FSDataInputStream in = fs.open(filePath);
            byte[] buffer = new byte[1024];
            OutputStream out = response.getOutputStream();
            int len = 0;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    // 删除文件
    @RequestMapping("/delete")
    public RedirectView deleteFile(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        try {
            // 获取文件在 HDFS 上的路径
            Path filePath = new Path("/output/" + fileName);
            // 如果文件不存在，则直接返回
            if (!fs.exists(filePath)) {
                return new RedirectView("/disk_list.html"); // 返回到文件列表页面
            }
            //如果文件存在就进行删除
            fs.delete(filePath);
            // 删除文件后重定向到当前页面，实现页面刷新
            return new RedirectView("/disk_list.html"); // 返回到文件列表页面
        } catch (Exception ex) {
            // 删除文件后重定向到当前页面，实现页面刷新
            return new RedirectView("/disk_list.html"); // 返回到文件列表页面
        }
    }
    // 查看文件
    @RequestMapping("/list")
    public AjaxResult listFiles() throws IOException {
        // 文件都在/optput
        Path dirPath = new Path("/output");
        if (!fs.exists(dirPath)) {
            return AjaxResult.fail(-2,"当前无文件存在!");
        }
        FileStatus[] fileStatuses = fs.listStatus(dirPath);
        List<Map<String, Object>> fileList = new ArrayList<>(fileStatuses.length);
        for (FileStatus fileStatus : fileStatuses) {
            if (fileStatus.isDirectory()) {
                // 跳过目录
                continue;
            }
            Map<String, Object> fileInfo = new HashMap<>();
            Path filePath = fileStatus.getPath();
            String fileName = filePath.getName();
            //获取文件的大小
            long fileSize = fileStatus.getLen();
            int fileSizeInKB = (int) Math.ceil(fileSize / 1024.0);
            String fileSizeStr = String.format("%d.%02d KB", fileSizeInKB / 100, fileSizeInKB % 100);
            //获取文件的上传时间
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long fileModificationTime = fileStatus.getModificationTime();
            String fileModificationTimeStr = dateFormat.format(fileModificationTime);
            //将结果添加到map中
            fileInfo.put("fileName", fileName);
            fileInfo.put("fileSize", fileSizeStr);
            fileInfo.put("uploadTime", fileModificationTimeStr);
            fileList.add(fileInfo);
        }
        return AjaxResult.success(fileList);
    }
}
