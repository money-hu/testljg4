package com.example.demo.common;

import lombok.Data;
import java.io.Serializable;


/*
 * 该代码定义了一个通用的模板类 AjaxResult，用于实现统一格式的数据响应
 * */
@Data
public class AjaxResult implements Serializable {
    //支持序列化接口
    //返回的状态码
    private Integer code;
    //返回的描述信息
    private String msg;
    //返回的数据本体
    private Object data;

    //定义成功返回的方法
    public static AjaxResult success(Object data){
        //请求成功之后 调用这个方法就可以统一格式返回
        //但是呢有的时候希望code msg可以自定义 所以可以用方法重载
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(200);
        ajaxResult.setMsg("");
        ajaxResult.setData(data);
        return ajaxResult;
    }

    public static AjaxResult success(Integer code,Object data){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg("");
        ajaxResult.setData(data);
        return ajaxResult;
    }

    public static AjaxResult success(Integer code,String msg,Object data){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(data);
        return ajaxResult;
    }


    //定义请求失败的返回数据的方法
    public static AjaxResult fail(Integer code,String msg){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(null);//失败则没有数据返回
        return ajaxResult;
    }

    public static AjaxResult fail(Integer code,String msg,Object data){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(data);
        return ajaxResult;
    }
}
